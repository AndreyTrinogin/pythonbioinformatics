
def get_ktuples(rmap, k):
    import collections
    length = len(rmap)
    defdict = collections.defaultdict(list)

    for i in range(length - k + 1):
        mlist = list()
        for j in range(i, i + k):
            mlist.append(rmap[j])
        mtuple = tuple(mlist)
        defdict[mtuple].append(i)

    return defdict


def feasible_match(rmap2, rmap1):
    import math
    s1 = sum(rmap1)
    s2 = sum(rmap2)

    res = math.fabs(s1 - s2)
    mults = map(lambda x: 0.02 * x, rmap2)
    squares = map(lambda x: x * x, mults)
    squares_sum = sum(squares)
    squares_sqrt = math.sqrt(squares_sum)
    res /= squares_sqrt

    return res


def get_locations(ref, query):
    c = 3.0
    mlist = list()
    for i in range(len(query) - 2, len(query) + 1):
        ktuples = get_ktuples(ref, i)
        for x in ktuples.keys():
            if feasible_match(x, query) < c:
                mlist.append((ktuples[x][0], len(x)))

    return sorted(mlist)


# Test for get_locations
# ref = [23.11, 2.82, 28.85, 3.16, 7.65, 6.45,
#        2.05, 6.86, 1.33, 8.58, 11.67, 23.22, 2.98,
#        28.9, 3.12, 7.41, 6.65, 2.06, 6.87, 1.32, 8.58,
#        11.65, 23.22, 2.88, 28.84, 3.52, 7.48, 6.84,
#        2.05, 6.88, 1.34, 8.57, 11.67, 23.15, 2.86, 1.32,
#        30.49, 7.42, 6.37]
# q = [2.027, 6.845, 1.305, 8.56, 11.63]
# print(get_locations(ref, q))


def align(rmap, ref, miss_penalty, delta):
    from math import inf

    sm = [[0.0 for i in range(len(ref) + 1)] for j in range(len(rmap) + 1)]
    for i in range(1, len(rmap) + 1):
        sm[i][0] = inf
    for j in range(1, len(ref) + 1):
        sm[0][j] = inf
    sm[0][0] = 0.0

    for s in range(1, len(rmap) + 1):
        for t in range(1, len(ref) + 1):
            min_value = inf
            for k in range(max(0, s - delta), s):
                for l in range(max(0, t - delta), t):
                    temp_ind = s - k + t - l
                    temp_h2 = feasible_match(ref[l:t], rmap[k:s]) ** 2.0
                    temp_value = miss_penalty * temp_ind + temp_h2 + sm[k][l]
                    if temp_value < min_value:
                        min_value = temp_value
            sm[s][t] = min_value
    return sm[len(rmap)][len(ref)]

# Test for align
# print(align([22.98, 2.93, 1.87, 27.14, 10.69, 6.49],
#             [23.13, 3.03, 1.78, 27.13, 3.06, 7.29, 23.93],
#             -1, 2))


# TODO: problem with packs import
# def m_scores(scores):
#     import numpy as np
#     m_a = np.median(scores)
#     s1 = []
#     s2 = []
#     for x in scores:
#         s1.append(abs(x-m_a))
#         s2.append(x - m_a)
#     mad_a = np.median(s1)
#     for i in range(len(s2)):
#         s2[i] = round(s2[i]/mad_a, 2)
#     return s2

# TODO: finisn this fun and 4th block
def best_alignment(ref, query, miss_penalty=-1, delta=2):
    pass
