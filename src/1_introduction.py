
# 1st task
# print(input())

# 2nd task
# print(len(input()))

# 3rd task
# md = dict()
# md['A'] = 0
# md['G'] = 0
# md['C'] = 0
# md['T'] = 0
# for x in input():
#     md[x] += 1
# print("A", md['A'])
# print("G", md['G'])
# print("C", md['C'])
# print("T", md['T'])

# 4th task
# str1 = input()
# str2 = input()
#
# if (str1.find(str2) != -1):
#     print("Yes")
# else:
#     print("No")

# 5th task
# str1 = input()
# md = dict()
# md['A'] = 0
# md['G'] = 0
# md['C'] = 0
# md['T'] = 0
# for x in str1:
#     md[x] += 1
# length = len(str1)
# print("A", round(md['A'] / length, 2))
# print("G", round(md['G'] / length, 2))
# print("C", round(md['C'] / length, 2))
# print("T", round(md['T'] / length, 2))


# 6th task
def union(dna1, dna2):
    ml = []
    for x in range(len(dna1)):
        if not x % 2:
            ml.append(dna1[x])
        else:
            ml.append(dna2[x])
    return "".join(ml)


# test for 6th task
# dna1 = "GTTGGGGATTGTAGC"
# dna2 = "CTCTACAATCTGGCC"
# print(union(dna1, dna2))
