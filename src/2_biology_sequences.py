
def transcription_1(dna):
    return dna.replace("T","U")


def translation(mrna):
    translation_matrix = {
        'UUU': 'Phe', 'UUC': 'Phe', 'UCU': 'Ser', 'UCC': 'Ser',
        'UAU': 'Tyr', 'UAC': 'Tyr', 'UGU': 'Cys', 'UGC': 'Cys',
        'UUA': 'Leu', 'UCA': 'Ser', 'UUG': 'Leu', 'UCG': 'Ser',
        'UGG': 'Trp', 'CUU': 'Leu', 'CUC': 'Leu', 'CCU': 'Pro',
        'CAU': 'His', 'CAC': 'His', 'CGU': 'Arg', 'CGC': 'Arg',
        'CUA': 'Leu', 'CUG': 'Leu', 'CCA': 'Pro', 'CCG': 'Pro',
        'CAA': 'Gln', 'CAG': 'Gln', 'CGA': 'Arg', 'CGG': 'Arg',
        'AUU': 'Ile', 'AUC': 'Ile', 'ACU': 'Thr', 'ACC': 'Thr',
        'AAU': 'Asn', 'AAC': 'Asn', 'AGU': 'Ser', 'AGC': 'Ser',
        'AUA': 'Ile', 'ACA': 'Thr', 'AAA': 'Lys', 'AGA': 'Arg',
        'AUG': 'Met', 'ACG': 'Thr', 'AAG': 'Lys', 'AGG': 'Arg',
        'GUU': 'Val', 'GUC': 'Val', 'GCU': 'Ala', 'GCC': 'Ala',
        'GAU': 'Asp', 'GAC': 'Asp', 'GGU': 'Gly', 'GGC': 'Gly',
        'GUA': 'Val', 'GUG': 'Val', 'GCA': 'Ala', 'GCG': 'Ala',
        'GAA': 'Glu', 'GAG': 'Glu', 'GGA': 'Gly', 'GGG': 'Gly',
        'CCC': 'Pro'}
    result = list()
    i = 0
    while i < len(mrna):
        result.append(translation_matrix.get(mrna[i:i + 3], ''))
        i += 3

    return ' '.join(result)


def transcription(*dnas):
    result = list()
    for dna in dnas:
        result.append(dna.replace("T","U"))
    return result


def complement(dna):
    trans = str.maketrans("ACGT", "TGCA")
    return str.translate(dna, trans)


def maxValue(x, *funcs):
    max_val = funcs[0](x)
    for f in funcs:
        if f(x) > max_val:
            max_val = f(x)
    return max_val


def medNumFrag(file_name):
    file = open(file_name, 'r')
    sum = 0
    num = 0
    for line in file:
        lst = line.split()
        sum += len(lst)
        num += 1
    file.close()
    return sum / num


def sumSquares(func1, func2):
    def inner(*args):
        return func1(*args) ** 2 + func2(*args) ** 2
    return inner


def curry(f, *args):
    def inner(*args):
        return f(*args)
    return inner
