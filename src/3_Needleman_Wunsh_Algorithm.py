
def hamming(seq1, seq2):
    res = 0
    for a, b in zip(seq1, seq2):
        if a != b:
            res += 1
    return res


def init(rows, cols, gap_penalty=10):
    matrix = [[float(0) for i in range(cols + 1)] for j in range(rows + 1)]
    matrix[0] = [float(j * -gap_penalty) for j in range(0, cols + 1)]
    for i in range(0, rows + 1):
        matrix[i][0] = float(i * -gap_penalty)
    return matrix


def get_new_score(up, left, middle,
                  matched, gap_penalty, match, mismatch):

    s = match if matched else mismatch
    a = middle + s
    b = left - gap_penalty
    c = up - gap_penalty
    return float(max(a, b, c))


def get_alignment(top_seq, bottom_seq, sm,
                  gap_penalty, match, mismatch):

    aligned1 = list()
    aligned2 = list()

    i = len(top_seq)
    j = len(bottom_seq)

    while j > 0 or i > 0:
        up = sm[i - 1][j] - gap_penalty
        left = sm[i][j - 1] - gap_penalty
        middle = sm[i - 1][j - 1] + (lambda x, y: match if x == y else mismatch)(top_seq[i - 1], bottom_seq[j - 1])
        if up >= middle and up >= left:
            aligned1.append(top_seq[i - 1])
            aligned2.append('-')
            i -= 1
        elif left >= middle and left >= up:
            aligned1.append('-')
            aligned2.append(bottom_seq[j - 1])
            j -= 1
        elif middle >= left and middle >= up:
            aligned1.append(top_seq[i - 1])
            aligned2.append(bottom_seq[j - 1])
            i -= 1
            j -= 1

    return "{}\n{}".format(''.join(aligned2[::-1]), ''.join(aligned1[::-1]))


def align(top_seq, bottom_seq, gap_penalty=10,
          match=2, mismatch=-1):

    top_len = len(top_seq)
    bottom_len = len(bottom_seq)
    m = init(top_len, bottom_len, gap_penalty)

    for i in range(1, top_len + 1):
        for j in range(1, bottom_len + 1):
            m[i][j] = get_new_score(m[i - 1][j], m[i][j - 1], m[i - 1][j - 1],
                                    1 if top_seq[i - 1] == bottom_seq[j - 1] else 0,
                                    gap_penalty, match, mismatch)

    score = m[top_len][bottom_len]
    alignment = get_alignment(top_seq, bottom_seq, m,
                              gap_penalty, match, mismatch)

    return alignment, score


print(align('AGTGTCGGCT',
            'ACTTCTACCCCAGC',
            1.399, 2.2168, -4.4499))
