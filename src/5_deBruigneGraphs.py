
def rev(kt):
    return kt[::-1]


def ktuples(seq, k):
    return [seq[i-k:i] for i in range(k, len(seq) + 1)]


def fw_ktuples(kt):
    return [kt[1::] + x for x in "ACGT"]


def add_in_graph(mgraph, elem1, elem2):
    if elem2 not in mgraph[elem1]:
        mgraph[elem1].append(elem2)
    if elem1 not in mgraph[elem2]:
        mgraph[elem2].append(elem1)


def build_graph(seqs):

    from collections import defaultdict
    graph = defaultdict(list)

    for seq1 in seqs:
        for seq2 in seqs:
            if seq1 == seq2:
                continue
            if seq1 in fw_ktuples(seq2):
                add_in_graph(graph, seq1, seq2)
            if seq1 in fw_ktuples(rev(seq2)):
                add_in_graph(graph, seq1, seq2)
            if rev(seq1) in fw_ktuples(seq2):
                add_in_graph(graph, seq1, seq2)

    return graph


def append_in_order(seq, order):
    if (order[len(order) - 1][1:]) != seq[0:-1]:
        order.append(rev(seq))
    else:
        order.append(seq)


def build_seq_order(seqs, mgraph):

    seq = list(filter(lambda x: len(mgraph[x]) == 1, mgraph))[0]

    visited = list()
    visited.append(seq)

    order = list()
    order.append(seq)

    next_seq = mgraph[seq][0]
    if seq[1::] != next_seq[0:-1]:
        if seq[1::] != rev(next_seq)[0:-1]:
            order[0] = order[0][::-1]

    while len(order) < len(seqs):
        next_list = mgraph[seq]
        for s in next_list:
            if s not in visited:
                next_seq = s
                break
        seq = next_seq
        visited.append(seq)
        append_in_order(seq, order)

    return order


def build_initial_seq(order):

    initial = list()
    initial.append(order[0])
    for x in order[1::]:
        initial.append(x[len(x) - 1])

    return "".join(initial)


def build_contig_ideal(seqs):

    graph = build_graph(seqs)
    order = build_seq_order(seqs, graph)
    initial = build_initial_seq(order)
    return initial


seqs1 = ["GTCTAC",
        "ATCTGC",
        "TGCTAG",
        "ATCGTC",
        "GCATCT",
        "TCGTCT"]

seqs2 = ["GCATTCG",
        "TACGATC",
        "TAGCATT",
        "CAGCTTA",
        "AGCTTAC",
        "CTTACGA",
        "CCAGCTT"]

seqs3 = ["TGGAATTGACCAGGACGA",
        "TAGCAGAGCAGGACCAGT",
        "GACCAGGACGAGACGATT",
        "GAATTGACCAGGACGAGA",
        "CAGGTGGAATTGACCAGG",
        "AATTGACCAGGACGAGAC",
        "GAGCAGGACCAGTTAAGG",
        "GTGGAATTGACCAGGACG",
        "GACCAGTTAAGGTGGACT",
        "ACCAGTTAAGGTGGACTA",
        "GCAGAGCAGGACCAGTTA",
        "ACCAGGACGAGACGATTC",
        "AGGTGGAATTGACCAGGA",
        "CCAGTTAAGGTGGACTAA",
        "CAATCAGGTGGAATTGAC",
        "AGCAGAGCAGGACCAGTT",
        "CAGGACCAGTTAAGGTGG",
        "GCTTAGCAGAGCAGGACC"]

res = build_contig_ideal(seqs3)
print('        ' + res)
# print('correct ' + 'GCTTAGCAGAGCAGGACCAGTTAAGGTGGACTAAC'[::-1])
print('correct ' + 'GCTTAGCAGAGCAGGACCAGTTAAGGTGGACTAAC')
# print('correct ' + 'GCATCTGCTAG'[::-1])
# print('correct ' + 'GCATCTGCTAG')